import database from "../config/database.js";

function sqlQuery(sql, post) {
  return new Promise((resolve, reject) => {
    database.query(sql, post, (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result);
      }
    });
  });
}

export default sqlQuery;
