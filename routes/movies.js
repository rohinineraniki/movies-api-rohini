import express from "express";
import verifyJwtToken from "../middleware/jwtToken.js";

import showError from "./showError.js";
import validateAddMovie from "../middleware/validateAddMovie.js";
import addMovieSchema from "../schema/addMovieSchema.js";

import validateId from "../middleware/validateId.js";
import getMovieSchema from "../schema/getMovieSchema.js";

import validateRole from "../middleware/validateRole.js";

import { PrismaClient } from "@prisma/client";
import updateMovieSchema from "../schema/updateMovieSchema.js";
const prisma = new PrismaClient();

const router = express.Router();

//GET ALL MOVIES

router.get("/", verifyJwtToken, async (req, res, next) => {
  try {
    let result = await prisma.all_movies.findMany();

    res.json({ movies: result });
  } catch (err) {
    console.log(err);
    next(err);
  }
});

//GET  MOVIE BY ID

router.get(
  `/:movieId`,

  verifyJwtToken,
  getMovieSchema,

  validateId,
  async (req, res, next) => {
    let movieId = req.params.movieId;

    try {
      let result = await prisma.all_movies.findUnique({
        where: {
          id: Number(movieId),
        },
      });

      if (!result) {
        next(
          showError(404, "Requested id not found, Please try with another id")
        );
      } else {
        res.json({ "Movie for selected id": result });
      }
    } catch (err) {
      next(err);
    }
  }
);

//ADD NEW MOVIE

router.post(
  "/",
  verifyJwtToken,
  validateRole,

  addMovieSchema,
  validateAddMovie,
  async (req, res, next) => {
    let bodyData = req.body;

    try {
      let result = await prisma.all_movies.create({
        data: bodyData,
      });

      res.json({
        message: "Movie data inserted successfully",
        "Inserted Data": result,
      });
    } catch (err) {
      console.log(err);
      next(err);
    }
  }
);

//UPDATE MOVIE BY ID

router.put(
  `/:movieId`,

  verifyJwtToken,
  validateRole,

  updateMovieSchema,
  validateAddMovie,

  async (req, res, next) => {
    let movieId = req.params.movieId;

    try {
      let result = await prisma.all_movies.update({
        where: {
          id: Number(movieId),
        },
        data: req.body,
      });
      console.log("in put", result);
      if (result.affectedRows === 0) {
        next(showError(400, "Given id not matched with existing data"));
      } else {
        res.json({
          message: "Movie details updated!",
          "Updated movie data": result,
        });
      }
    } catch {
      console.log(err);
      next(err);
    }
  }
);

// DELETE MOVIE BY ID

router.delete(
  `/:movieId`,

  verifyJwtToken,
  validateRole,

  getMovieSchema,
  validateId,
  async (req, res, next) => {
    try {
      let result = await prisma.all_movies.delete({
        where: {
          id: Number(req.params.movieId),
        },
      });

      res.json({
        message: "Movie deleted successfully",
        "Deleted movie": result,
      });
    } catch (err) {
      next(err);
    }
  }
);

export default router;
