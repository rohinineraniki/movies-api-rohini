import { body } from "express-validator";

console.log("optional");
const updateMovieSchema = [
  body("Title").optional().trim().exists({ checkFalsy: true }),
  body("Description").optional().trim().exists({ checkFalsy: true }),
  body("Runtime").optional().isInt({ min: 1 }),

  body("Genre").optional().isAlpha("en-IN", { ignore: " " }),
  body("Rating").optional().isFloat({ min: 0.0, max: 10.0 }),
  body("Metascore").optional().isString(),

  body("Votes").optional().isInt({ min: 0 }),
  body("Gross_Earning_in_Mil").optional().isString(),
  body("Director").optional().isAlpha("en-IN", { ignore: " " }),

  body("Actor").optional().isAlpha("en-IN", { ignore: " " }),
  body("Year").optional().isInt({ min: 1000 }),

  body().custom((value, { req }) => {
    const bodyLength = Object.keys(req.body).length;
    const bodyFields = Object.keys(req.body);
    const all_columns = [
      "Title",
      "Description",
      "Runtime",
      "Genre",
      "Rating",
      "Metascore",
      "Votes",
      "Gross_Earning_in_Mil",
      "Director",
      "Actor",
      "Year",
    ];

    if (bodyLength > 0 && bodyLength <= 11) {
      const checkValidField = bodyFields.filter((each) => {
        return all_columns.includes(each);
      });
      if (checkValidField.length === bodyFields.length) {
        return true;
      } else {
        throw new Error("Invalid body inputs");
      }
    } else {
      throw new Error("Invalid body length");
    }
  }),
];

export default updateMovieSchema;
