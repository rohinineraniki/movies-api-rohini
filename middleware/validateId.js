import { validationResult } from "express-validator";
import showError from "../routes/showError.js";

function validateId(req, res, next) {
  if (validationResult(req).errors.length > 0) {
    next(showError(400, "Please enter valid id"));
  } else {
    next();
  }
}

export default validateId;
