import express from "express";
import router from "./routes/movies.js";

import createError from "http-errors";
import authRouter from "./routes/userSignUp.js";

import authLogin from "./routes/userLogin.js";
import showError from "./routes/showError.js";

const app = express();
app.use(express.json({ extended: false }));

app.use(express.urlencoded({ extended: false }));

const PORT = process.env.PORT || 3000;

app.get("/", (req, res, next) => {
  res.json({
    message: "This is home page please move to /api/movies to see movies data",
  });
});

app.use("/api/movies", router);
app.use("/api/auth/signup", authRouter);
app.use("/api/auth/login", authLogin);

app.use((req, res, next) => {
  next(createError(404));
});

app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({ message: showError.message || "Error" });
});

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}...`);
});

export default app;
