import jwt from "jsonwebtoken";
import showError from "../routes/showError.js";

const verifyJwtToken = (req, res, next) => {
  let authHeader = req.headers["authorization"];
  let authToken;
  if (authHeader !== undefined) {
    authToken = authHeader.split(" ")[1];
  }

  if (authToken === undefined) {
    next(
      showError(
        401,
        "Invalid access token (Please login with valid credentials in /api/auth/login path , if new user first need to signup /api/auth/signup path)"
      )
    );
  }
  jwt.verify(authToken, process.env.TOKEN, (err, payload) => {
    if (err) {
      next(showError(500, "Got Invalid access token"));
    } else {
      console.log("payload", payload);
      req.role_type = payload.role_type;
      next();
    }
  });
};

export default verifyJwtToken;
