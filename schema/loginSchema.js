import { body } from "express-validator";

const loginSchema = [
  body("username")
    .exists({ checkFalsy: true, checkNull: true })
    .withMessage("Please enter username"),

  body("password")
    .exists({ checkFalsy: true, checkNull: true })
    .withMessage("Please enter password"),

  body().custom((value, { req }) => {
    const bodyLength = Object.keys(req.body).length;

    if (bodyLength === 2) {
      return true;
    } else {
      throw new Error("Invalid body length");
    }
  }),
];

export default loginSchema;
