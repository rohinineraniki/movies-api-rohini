import { body } from "express-validator";

const signupSchema = [
  body("name").isAlpha("en-IN", { ignore: " " }),
  body("username").exists({ checkFalsy: true }).trim().isLength({ min: 3 }),

  body("password").isStrongPassword(),
  body("email").isEmail(),
  body("role").isIn(["user"]).withMessage("role should be user"),

  body().custom((value, { req }) => {
    const bodyLength = Object.keys(req.body).length;

    if (bodyLength === 5) {
      return true;
    } else {
      throw new Error("Invalid body length");
    }
  }),
];
export default signupSchema;
