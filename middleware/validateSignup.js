import { validationResult } from "express-validator";
import showError from "../routes/showError.js";

function validateSignup(req, res, next) {
  if (validationResult(req).errors.length > 0) {
    next(
      showError(400, `Please enter valid fields, see instructions below`, {
        name: "must contain alphabets",
        username: "not empty value",
        password:
          "Must include atleast 1 upper,lower case letter, special character,number",
        email: "valid email",
        role: "user",
      })
    );
  } else {
    next();
  }
}

export default validateSignup;
