import express from "express";
import bcypt from "bcrypt";

import jsonwebtoken from "jsonwebtoken";
import showError from "./showError.js";

import loginSchema from "../schema/loginSchema.js";
import validationLogin from "../middleware/validateLogin.js";

import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

const authLogin = express.Router();

authLogin.post("/", loginSchema, validationLogin, async (req, res, next) => {
  const post = req.body;
  const { username, password } = post;

  try {
    let result = await prisma.all_users.findMany({
      where: {
        username: username,
      },
    });

    if (!result) {
      next(showError(400, "Incorrect Username"));
    } else {
      let passwordMatch = await bcypt.compare(password, result[0].password);

      if (passwordMatch === true) {
        const resultPrisma = await prisma.all_users.findMany({
          select: {
            role_id: true,
            username: true,
            roles: {
              select: {
                id: true,
                role: true,
              },
            },
          },
          where: {
            username: username,
          },
        });

        const payload = {
          username: username,
          role_type: resultPrisma[0].roles.role,
        };

        const jwtToken = jsonwebtoken.sign(payload, process.env.TOKEN);

        res.json({ token: `${jwtToken}` });
      } else {
        next(showError(400, "Incorrect password"));
      }
    }
  } catch (err) {
    console.log(err);
    next(showError(500, "Error in getting data, Please try again!!"));
  }
});

export default authLogin;
