import showError from "../routes/showError.js";

function validateRole(req, res, next) {
  if (req.role_type === "admin") {
    next();
  } else {
    next(showError(401, "Unauthorized to view content"));
  }
}

export default validateRole;
