import express from "express";
import bcrypt, { hash } from "bcrypt";

import showError from "./showError.js";
import signupSchema from "../schema/signupSchema.js";

import validateSignup from "../middleware/validateSignup.js";

import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

const authRouter = express.Router();

authRouter.post("/", signupSchema, validateSignup, async (req, res, next) => {
  const post = req.body;

  const { name, username, password, email, role } = post;

  try {
    let checkUserExist = await prisma.all_users.findMany({
      where: {
        username: username,
      },
    });

    let checkEmailExist = await prisma.all_users.findMany({
      where: {
        email: email,
      },
    });

    if (checkUserExist.length !== 0) {
      next(
        showError(
          400,
          "Username already exists, Please try with another user name"
        )
      );
    } else if (checkEmailExist.length !== 0) {
      next(
        showError(400, "Email already exists, Please try with another mail")
      );
    } else {
      let hashedPassword = await bcrypt.hash(password, 10);

      try {
        let result = await prisma.all_users.create({
          data: {
            name: name,
            username: username,
            password: hashedPassword,
            email: email,
            role_id: 1,
          },
        });

        res.json({ Message: "User Registered Successfully!!", data: post });
      } catch (err) {
        next(
          showError(
            500,
            "Error in inserting user data in all_users table, Please try again!"
          )
        );
      }
    }
  } catch (err) {
    console.log(err);
    next(showError(500, "Error in getting all users data, please try again!"));
  }
});

export default authRouter;
