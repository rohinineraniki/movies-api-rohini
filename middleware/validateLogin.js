import { validationResult } from "express-validator";
import showError from "../routes/showError.js";

function validationLogin(req, res, next) {
  if (validationResult(req).errors.length > 0) {
    let errorMsgs = validationResult(req).errors.map((each) => {
      return each.msg;
    });

    console.log(errorMsgs);
    next(
      showError(400, "Enter valid inputs it is like", {
        username: "",
        password: "",
      })
    );
  } else {
    next();
  }
}
export default validationLogin;
