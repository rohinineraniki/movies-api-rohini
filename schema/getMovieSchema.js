import { param } from "express-validator";

const getMovieSchema = [param("movieId").isInt()];

export default getMovieSchema;
