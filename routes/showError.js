function showError(status, message, format) {
  let errorObj;
  format
    ? (errorObj = {
        status: status,
        message: [message, format],
      })
    : (errorObj = {
        status: status,
        message: message,
      });
  return errorObj;
}

export default showError;
