import mysql from "mysql";
import dotenv from "dotenv";

dotenv.config();

const database = mysql.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
});

database.getConnection(function (err) {
  if (err) {
    console.error("error in connecting: ", err);
    throw err;
  } else {
    console.log("connected to database");
  }
});

export default database;
