import { body, validationResult, check } from "express-validator";
import showError from "../routes/showError.js";

function validateAddMovie(req, res, next) {
  if (validationResult(req).errors.length > 0) {
    next(
      showError(400, "Please enter valid body inputs like ", {
        Title: "Enter Title",
        Description: "Enter description",
        Runtime: "Enter total runtime",

        Genre: "Enter genre",
        Rating: "Enter number between 1 to 10",
        Metascore: "Enter ",

        Votes: "Enter number of votes",
        Gross_Earning_in_Mil: "Enter gross earning",
        Director: "Enter director name",

        Actor: "Enter actor name",
        Year: "Enter year",
      })
    );
  } else {
    next();
  }
}

export default validateAddMovie;
